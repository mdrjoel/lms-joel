﻿namespace LMS
{
    partial class EventSetter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EventSetter));
            this.btnLoadCardReader = new System.Windows.Forms.Button();
            this.tbxCourseCode = new System.Windows.Forms.TextBox();
            this.tbxLessonCode = new System.Windows.Forms.TextBox();
            this.tbxEventCode = new System.Windows.Forms.TextBox();
            this.chkMandatory = new System.Windows.Forms.CheckBox();
            this.calStartDate = new System.Windows.Forms.MonthCalendar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cboHourStart = new System.Windows.Forms.ComboBox();
            this.cboMinutesStart = new System.Windows.Forms.ComboBox();
            this.cboMinuteEnd = new System.Windows.Forms.ComboBox();
            this.cboHourEnd = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbxOutput = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.picFolder = new System.Windows.Forms.PictureBox();
            this.tbxLocation = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lblWorking = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picFolder)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLoadCardReader
            // 
            this.btnLoadCardReader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnLoadCardReader.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoadCardReader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnLoadCardReader.Location = new System.Drawing.Point(450, 388);
            this.btnLoadCardReader.Margin = new System.Windows.Forms.Padding(4);
            this.btnLoadCardReader.Name = "btnLoadCardReader";
            this.btnLoadCardReader.Size = new System.Drawing.Size(240, 37);
            this.btnLoadCardReader.TabIndex = 12;
            this.btnLoadCardReader.Text = "Start registration";
            this.btnLoadCardReader.UseVisualStyleBackColor = false;
            this.btnLoadCardReader.Click += new System.EventHandler(this.btnLoadCardReader_Click);
            // 
            // tbxCourseCode
            // 
            this.tbxCourseCode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbxCourseCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbxCourseCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxCourseCode.ForeColor = System.Drawing.Color.White;
            this.tbxCourseCode.Location = new System.Drawing.Point(30, 43);
            this.tbxCourseCode.Name = "tbxCourseCode";
            this.tbxCourseCode.Size = new System.Drawing.Size(216, 24);
            this.tbxCourseCode.TabIndex = 1;
            // 
            // tbxLessonCode
            // 
            this.tbxLessonCode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbxLessonCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbxLessonCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxLessonCode.ForeColor = System.Drawing.Color.White;
            this.tbxLessonCode.Location = new System.Drawing.Point(30, 106);
            this.tbxLessonCode.Name = "tbxLessonCode";
            this.tbxLessonCode.Size = new System.Drawing.Size(216, 24);
            this.tbxLessonCode.TabIndex = 2;
            // 
            // tbxEventCode
            // 
            this.tbxEventCode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbxEventCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbxEventCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxEventCode.ForeColor = System.Drawing.Color.White;
            this.tbxEventCode.Location = new System.Drawing.Point(31, 166);
            this.tbxEventCode.Name = "tbxEventCode";
            this.tbxEventCode.Size = new System.Drawing.Size(216, 24);
            this.tbxEventCode.TabIndex = 3;
            // 
            // chkMandatory
            // 
            this.chkMandatory.AutoSize = true;
            this.chkMandatory.Checked = true;
            this.chkMandatory.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkMandatory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkMandatory.ForeColor = System.Drawing.Color.White;
            this.chkMandatory.Location = new System.Drawing.Point(598, 43);
            this.chkMandatory.Name = "chkMandatory";
            this.chkMandatory.Size = new System.Drawing.Size(100, 22);
            this.chkMandatory.TabIndex = 10;
            this.chkMandatory.Text = "Mandatory";
            this.chkMandatory.UseVisualStyleBackColor = true;
            // 
            // calStartDate
            // 
            this.calStartDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.calStartDate.ForeColor = System.Drawing.Color.White;
            this.calStartDate.Location = new System.Drawing.Point(287, 43);
            this.calStartDate.Name = "calStartDate";
            this.calStartDate.TabIndex = 9;
            this.calStartDate.TitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.calStartDate.TitleForeColor = System.Drawing.Color.Gray;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(27, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 18);
            this.label1.TabIndex = 6;
            this.label1.Text = "Course code";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(28, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 18);
            this.label2.TabIndex = 7;
            this.label2.Text = "Lesson code";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(28, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 18);
            this.label3.TabIndex = 8;
            this.label3.Text = "Event code";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(284, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 18);
            this.label4.TabIndex = 9;
            this.label4.Text = "Date";
            // 
            // cboHourStart
            // 
            this.cboHourStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cboHourStart.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboHourStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboHourStart.ForeColor = System.Drawing.Color.White;
            this.cboHourStart.Location = new System.Drawing.Point(30, 313);
            this.cboHourStart.Name = "cboHourStart";
            this.cboHourStart.Size = new System.Drawing.Size(100, 26);
            this.cboHourStart.TabIndex = 5;
            // 
            // cboMinutesStart
            // 
            this.cboMinutesStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cboMinutesStart.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboMinutesStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMinutesStart.ForeColor = System.Drawing.Color.White;
            this.cboMinutesStart.FormattingEnabled = true;
            this.cboMinutesStart.Location = new System.Drawing.Point(136, 313);
            this.cboMinutesStart.Name = "cboMinutesStart";
            this.cboMinutesStart.Size = new System.Drawing.Size(100, 26);
            this.cboMinutesStart.TabIndex = 6;
            // 
            // cboMinuteEnd
            // 
            this.cboMinuteEnd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cboMinuteEnd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboMinuteEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMinuteEnd.ForeColor = System.Drawing.Color.White;
            this.cboMinuteEnd.FormattingEnabled = true;
            this.cboMinuteEnd.Location = new System.Drawing.Point(136, 393);
            this.cboMinuteEnd.Name = "cboMinuteEnd";
            this.cboMinuteEnd.Size = new System.Drawing.Size(100, 26);
            this.cboMinuteEnd.TabIndex = 8;
            // 
            // cboHourEnd
            // 
            this.cboHourEnd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cboHourEnd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboHourEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboHourEnd.ForeColor = System.Drawing.Color.White;
            this.cboHourEnd.FormattingEnabled = true;
            this.cboHourEnd.Location = new System.Drawing.Point(30, 393);
            this.cboHourEnd.Name = "cboHourEnd";
            this.cboHourEnd.Size = new System.Drawing.Size(100, 26);
            this.cboHourEnd.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(27, 294);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 18);
            this.label6.TabIndex = 16;
            this.label6.Text = "Start time";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(27, 372);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 18);
            this.label7.TabIndex = 17;
            this.label7.Text = "End time";
            // 
            // tbxOutput
            // 
            this.tbxOutput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbxOutput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbxOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxOutput.ForeColor = System.Drawing.Color.White;
            this.tbxOutput.Location = new System.Drawing.Point(287, 318);
            this.tbxOutput.Name = "tbxOutput";
            this.tbxOutput.Size = new System.Drawing.Size(354, 24);
            this.tbxOutput.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(284, 297);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 18);
            this.label5.TabIndex = 19;
            this.label5.Text = "Output folder";
            // 
            // picFolder
            // 
            this.picFolder.BackColor = System.Drawing.Color.Transparent;
            this.picFolder.Image = global::LMS.Properties.Resources.BlackFolderIconHoriz;
            this.picFolder.Location = new System.Drawing.Point(652, 315);
            this.picFolder.Name = "picFolder";
            this.picFolder.Size = new System.Drawing.Size(46, 29);
            this.picFolder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picFolder.TabIndex = 20;
            this.picFolder.TabStop = false;
            this.picFolder.Click += new System.EventHandler(this.picFolder_Click);
            // 
            // tbxLocation
            // 
            this.tbxLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbxLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbxLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxLocation.ForeColor = System.Drawing.Color.White;
            this.tbxLocation.Location = new System.Drawing.Point(31, 227);
            this.tbxLocation.Name = "tbxLocation";
            this.tbxLocation.Size = new System.Drawing.Size(216, 24);
            this.tbxLocation.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(33, 206);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 18);
            this.label8.TabIndex = 22;
            this.label8.Text = "Location";
            // 
            // lblWorking
            // 
            this.lblWorking.AutoSize = true;
            this.lblWorking.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWorking.ForeColor = System.Drawing.Color.White;
            this.lblWorking.Location = new System.Drawing.Point(282, 400);
            this.lblWorking.Name = "lblWorking";
            this.lblWorking.Size = new System.Drawing.Size(0, 24);
            this.lblWorking.TabIndex = 23;
            // 
            // EventSetter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(779, 443);
            this.Controls.Add(this.lblWorking);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbxLocation);
            this.Controls.Add(this.picFolder);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbxOutput);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cboMinuteEnd);
            this.Controls.Add(this.cboHourEnd);
            this.Controls.Add(this.cboMinutesStart);
            this.Controls.Add(this.cboHourStart);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.calStartDate);
            this.Controls.Add(this.chkMandatory);
            this.Controls.Add(this.tbxEventCode);
            this.Controls.Add(this.tbxLessonCode);
            this.Controls.Add(this.tbxCourseCode);
            this.Controls.Add(this.btnLoadCardReader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "EventSetter";
            this.Text = "Enter course details....";
            this.Load += new System.EventHandler(this.EventSetter_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picFolder)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLoadCardReader;
        private System.Windows.Forms.TextBox tbxCourseCode;
        private System.Windows.Forms.TextBox tbxLessonCode;
        private System.Windows.Forms.TextBox tbxEventCode;
        private System.Windows.Forms.CheckBox chkMandatory;
        private System.Windows.Forms.MonthCalendar calStartDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboHourStart;
        private System.Windows.Forms.ComboBox cboMinutesStart;
        private System.Windows.Forms.ComboBox cboMinuteEnd;
        private System.Windows.Forms.ComboBox cboHourEnd;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbxOutput;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox picFolder;
        private System.Windows.Forms.TextBox tbxLocation;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblWorking;
    }
}