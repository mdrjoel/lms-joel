﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DapperExtensions;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using Microsoft.SqlServer;

namespace LMS.CardData
{

    public class Key : Attribute { }

    public class CourseDetails
    {
        public string CourseCode { get; set; }
        public string LessonCode { get; set; }
        public string EventCode { get; set; }
      
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public bool Mandatory { get; set; }

        public string OutputPath { get; set; }
        public string Location { get; set; }
    }

    public class CSVData
    {
        public string UserName { get; set; }
        public string CourseCode { get; set; }
        public string LessonCode { get; set; }
        public string EventCode { get; set; }
        public string Status { get; set; }
        public string Mandatory { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }

    public String Location { get; set; }


    }


    public class ActivityRecording
    {
        private const string c_DBConnection = @"Data Source=DC3-SQL2008-01\DEV;Initial Catalog=TrainingRegistration;Integrated Security=True;Pooling=False";

        public static bool RecordActivity(Registrations registration)
        {
            using (IDbConnection db = new SqlConnection(c_DBConnection))
            {
                try
                {
                    var result = db.Insert(registration);
                    return result !=0;
                   
                }
                catch { return false; }

            }
        }

        public static bool CanConnect()
        {
            bool connected;
            try
            {
                SqlConnection sq = new SqlConnection(c_DBConnection);
                sq.Open();
                connected = sq.State == ConnectionState.Open;
                sq.Close();
                sq.Dispose();
                return connected;
            }
            catch { return false; }


        }

    }

    

    public class Registrations
    {
        [Key]
        public int Id { get; set; }
        public DateTime DateTime { get; set; }
        public string CardNumber { get; set; }
        public string EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set;}
        public string Department { get; set; }
        public string Email { get; set; }
        public string DeviceID { get; set; }

    }
}
