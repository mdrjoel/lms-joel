﻿using LMS.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.CardData
{
    public class Card
    {
        public List<EmployeeData> EmployeesData { get; set; }
        public bool Connected { get; set; }
        public List<EmployeeData> GetData(string CardId)
        {
            try
            {



                using (eqcasEntities context = new eqcasEntities())
                {

                    var EmployeeDetails = context.cat_validation.Where(x => x.primarypin == CardId);

                    EmployeesData = new List<EmployeeData>();


                    if (EmployeeDetails.Count() > 0)
                    {
                        foreach (var ed in EmployeeDetails)
                        {
                            EmployeeData Employee = new EmployeeData();
                            Employee.CardNumber = ed.primarypin;
                            Employee.Description = ed.name;
                            Employee.TranslatedCardNumber = ed.primarypin;
                            //ed.TranslatedCardNumber = ReadingCard.ConvertNumber(ed.CSNDecimal);
                            EmployeesData.Add(Employee);
                        }
                    }



                }
            }
            catch (Exception ex)
            {
                Connected = false;
            }
            return EmployeesData;
        }
    }
}

 public class EmployeeData
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Description { get; set; }
        public string CardNumber { get; set; }
        public string CSNDecimal { get; set; }
        public string EmployeeNumber { get; set; }
        public string UserId { get; set; }
        public string Department { get; set; }
        public Bitmap Photo { get; set; }

        public string TranslatedCardNumber { get; set; }


    }
